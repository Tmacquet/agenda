export interface Article {
    id: number;
    nom: string;
    prix: number;
    quantite: number;
    image: string;
    description: string;
    id_categorie: number;
    created_at: string;
    updated_at: string;
    jointurecategorie: Jointurecategorie;
  }
  
 export  interface Jointurecategorie {
    id: number;
    nom: string;
    created_at?: any;
    updated_at?: any;
  }