import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from './types';

@Injectable({
  providedIn: 'root'
})



export class ArticleService {

  constructor(private httpClient: HttpClient) { }

  getArticle(ArticleId: string): Observable<Article> {
    return this.httpClient.get<Article>(Header + API + "/" + ArticleId);
  }

  getArticles(): Observable<Article[]>{
    return this.httpClient.get<Article[]>( Header+API);
  }
}

const API = "http://localhost/api/Article";


