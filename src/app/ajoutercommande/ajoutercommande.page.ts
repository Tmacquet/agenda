import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../types';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-ajoutercommande',
  templateUrl: './ajoutercommande.page.html',
  styleUrls: ['./ajoutercommande.page.scss'],
})
export class AjoutercommandePage implements OnInit {

ArticleListe: Observable<Article[]>;

  constructor(articleService: ArticleService) {
    this.ArticleListe = articleService.getArticles();

   }

  ngOnInit() {
  }

}
